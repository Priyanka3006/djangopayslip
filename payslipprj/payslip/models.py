from django.db import models

class Payslip(models.Model):
    employee_id = models.CharField(max_length=50)
    status = models.CharField(max_length=50)

    def __str__(self):
        return f"Payslip - Employee ID: {self.employee_id}, Status: {self.status}"
