from django.urls import path
# from .views import upload, validate_json
from .views import display

urlpatterns = [
    path('', display, name='display'),  # Handle the root URL with the upload view
#     path('validate-json/', validate_json, name='validate_json'),  # Handle the validate-json URL with the validate_json view
]
