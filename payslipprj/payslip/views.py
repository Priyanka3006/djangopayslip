# import json
# from django.http import JsonResponse
#
# def validate_json(request):
#     if request.method == 'POST':
#         # Get the JSON data from the request
#         json_data = request.POST.get('json_data')
#
#         # Process the JSON data and validate the rows
#         # Replace this with your own logic to process and validate the JSON data
#         # Here, we assume that the JSON data is a list of dictionaries representing rows
#
#         if json_data:
#             try:
#                 data_rows = json.loads(json_data)
#                 valid_rows = []
#                 invalid_rows = []
#
#                 for row in data_rows:
#                     if any(row.values()):
#                         valid_rows.append(row)
#                     else:
#                         invalid_rows.append(row)
#
#                 response_data = {
#                     'valid_rows': valid_rows,
#                     'invalid_rows': invalid_rows
#                 }
#
#                 return JsonResponse(response_data)
#             except json.JSONDecodeError:
#                 return JsonResponse({'error': 'Invalid JSON data.'}, status=400)
#         else:
#             return JsonResponse({'error': 'No JSON data provided.'}, status=400)
#     else:
#         return JsonResponse({'error': 'Invalid request method.'}, status=405)
#
# def upload(request):
#     if request.method == 'POST':
#         # Handle the POST request
#         file = request.FILES['file']
#
#         # Process the file and generate the context
#         # Replace this with your own logic to process the file and generate the context dictionary
#
#         context = {
#             'key1': 'value1',
#             'key2': 'value2',
#             # Other key-value pairs
#         }
#
#         return JsonResponse(context)
#     else:
#         # Handle the GET request
#         return JsonResponse({"error": "Invalid request method."}, statu
from django.shortcuts import render
def display(request):
    return render(request,"upload.html")


